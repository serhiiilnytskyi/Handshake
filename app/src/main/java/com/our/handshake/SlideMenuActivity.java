package com.our.handshake;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.our.handshake.Adapters.SlideMenuSwipeAdapter;


public class SlideMenuActivity extends FragmentActivity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide_menu_layout_10);

        viewPager = (ViewPager)findViewById(R.id.slide_view_pager);
        SlideMenuSwipeAdapter slideMenuSwipeAdapter = new SlideMenuSwipeAdapter(getSupportFragmentManager());
        viewPager.setAdapter(slideMenuSwipeAdapter);
    }
}
