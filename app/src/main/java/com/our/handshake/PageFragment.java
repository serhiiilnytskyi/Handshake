package com.our.handshake;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.caverock.androidsvg.SVGImageView;

public class PageFragment extends Fragment {

    private static int current = 0;

    public PageFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        current = getArguments().getInt("count");

        return inflater.inflate(R.layout.page_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FrameLayout layout = (FrameLayout) view.findViewById(R.id.slide_menu_fragment);
        SVGImageView svgImageView = new SVGImageView(getContext());
        svgImageView.setId(R.id.slideMenuSVG);
        ImageView indicator = new ImageView(getContext());

        switch (current) {
            case 0:
                svgImageView.setImageAsset("svg/slide_menu_1.svg");
                indicator.setImageResource(R.drawable.indicator_1);
                break;
            case 1:
                svgImageView.setImageAsset("svg/slide_menu_2.svg");
                indicator.setImageResource(R.drawable.indicator_2);
                break;
            case 2:
                svgImageView.setImageAsset("svg/slide_menu_3.svg");
                indicator.setImageResource(R.drawable.indicator_3);
                break;
            case 3:
                svgImageView.setImageAsset("svg/slide_menu_4.svg");
                indicator.setImageResource(R.drawable.indicator_4);
                break;
            default:
                throw new RuntimeException("bad view index");
        }

        //add svg image params
        svgImageView.setAdjustViewBounds(true);
        svgImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        //add indicator params
        LinearLayout indicatorLayout = new LinearLayout(getContext());
        indicatorLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        //todo convert px to dp
        lp.setMargins(0, 0, 0, 150);
        indicator.setLayoutParams(lp);
        indicatorLayout.addView(indicator);
        indicatorLayout.setLayoutParams(lp);

        //add layouts
        layout.addView(svgImageView,
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        layout.addView(indicatorLayout,
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        layout.addView(handshakesText(),
                new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
    }

    private LinearLayout handshakesText() {
        LinearLayout handShakesLayout = new LinearLayout(getContext());
        handShakesLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
        ImageView imageView = new ImageView(getContext());
        imageView.setImageResource(R.drawable.handshakes_ua);
        handShakesLayout.addView(imageView);
        return handShakesLayout;
    }

}
