package com.our.handshake;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in_layout_3);
        initFont();

    }

    private void initFont(){

        TextView emailTV = (TextView) findViewById(R.id.emailSignInET);
        TextView passwordTV = (TextView)findViewById(R.id.passwordEditET);
        TextView dontHaveAccTV = (TextView)findViewById(R.id.dontHaveAccTV);
        TextView forgotPassTV = (TextView)findViewById(R.id.forgotPassTF);
        TextView signUpTV = (TextView) findViewById(R.id.signUp);
        Button signInBtn = (Button)findViewById(R.id.signInBtn);

        Typeface typefaceLight = Typeface.create(Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Light.ttf"),Typeface.NORMAL);
        Typeface typefaceSemiBold = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-SemiBold.ttf");
        Typeface typefaceRegular =  Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");

        emailTV.setTypeface(typefaceLight);
        passwordTV.setTypeface(typefaceLight);
        dontHaveAccTV.setTypeface(typefaceRegular);
        forgotPassTV.setTypeface(typefaceRegular);
        signUpTV.setTypeface(typefaceSemiBold);
        signInBtn.setTypeface(typefaceSemiBold);

    }
}
